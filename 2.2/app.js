        function filtrateNumbers(value) {
            let resultArr = [];
            for (letter of value) {
                if (isNaN(letter)) {
                    resultArr.push(letter)
                }
            }
            return resultArr.join('')
        }

        function numberInsensitiveComparator(word1, word2) {
            let filtratedWord1 = filtrateNumbers(word1)
            let filtratedWord2 = filtrateNumbers(word2)
            if (filtratedWord1 > filtratedWord2) {
                return 1;
            }
            if (filtratedWord1 < filtratedWord2) {
                return -1;
            }
            return 0;
        };

        function superSort(value) {
            const lowerCase = value.toLocaleLowerCase();
            const words = lowerCase.split(' ')
            const sortedItem = words.sort(numberInsensitiveComparator)
            return sortedItem.join(' ')
        }
        console.log(superSort('mic09ha1el 4b5en6 michelle be4atr3ice'));
