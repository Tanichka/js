function findDifferentNumber(array) {
    for (let index = 0; index < array.length; index++) {
        let elem1 = array[index];
        let elem2 = array[index + 1];
        let elem3 = array[index + 2];
        if (elem1 === elem2 && elem1 !== elem3) {
            return elem3;
        } else if (elem1 === elem3 && elem1 !== elem2) {
            return elem2;
        } else if (elem2 === elem3 && elem2 !== elem1) {
            return elem1;
        }
    }
}
console.log(findDifferentNumber([1, 1, 3, 1, 1]));
